# clip-url - Extract URLs from copied portions of web pages

`clip-url` is a simple command line utility which extracts URLs from copied
portions of web pages.

You don't need to copy the HTML source of a web page - just copy the text
into the clipboard straight from your browser and run `clip-url`.

## Usage

Open up your terminal and type:

`clip-url`

Nothing else is needed. You can easily pipe the output to other terminal
utilities like `wget` or `curl`, or paste it into download managers
like `uGet`.

## License

`clip-url` is libre software released under the GNU Affero General Public
License version 3 (or any later version).
